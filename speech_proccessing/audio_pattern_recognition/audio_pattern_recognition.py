from __future__ import division
import warnings
warnings.filterwarnings("ignore")
import numpy as np
from scipy.io import wavfile
from numpy import linspace
from IPython import display
import matplotlib.pyplot as plt
from numpy.fft import fft, fftshift
import pyaudio
import wave
import sys
import math
from pandas import *
import os.path
import librosa

# tabeye Khaandan audio test va tabdil be energy
def khandanAudioTest():
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 48000
    RECORD_SECONDS = 2.10
    WAVE_OUTPUT_FILENAME = "./wavSignals/test.wav"

    print("Lotfan pas az zadan dokmeye Enter yek adad beyn 0 ta 9 begoo,\n \
shayad betonam ono be moaadel neveshtarish tabdid konam :)")
    input()
    print("*** Dar hale zabt kardan seda ... .")
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK, exception_on_overflow = False)
        frames.append(data)
    print("*** Payan zabt.")

    stream.stop_stream()
    stream.close()
    p.terminate()

    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()

    y, sr = librosa.load("./wavSignals/test.wav")
    return librosa.feature.mfcc(y, sr)

# Tarif tabeye dar ham pichesh zamani.
def dtw(template_name, mfccTest):
    rows, cols = (len(mfccTempl[template_name][0])+1, 
                  len(mfccTest[0])+1) 
    mat = [[0] * cols for _ in range(rows)] 

    #arzesh gozari avalieh matrix
    for i in range(0, len(mfccTempl[template_name][0])+1):
        for j in range(0, len(mfccTest[0])+1):
            mat[i][j] = float('inf')
            if i == 0 or j == 0:
                mat[i][j] = 0

    #print("before: )
    #print(DataFrame(mat))

    for i in range(0, len(mfccTempl[template_name][0])):
        for j in range(0, len(mfccTest[0])):
            curVal = 0
            for k in range(0, len(mfccTempl[template_name])):
                curVal += (abs(mfccTest[k][j] - 
                    mfccTempl[template_name][k][i])**2)
            curVal = math.sqrt(curVal)
            mat[i+1][j+1] = (curVal + min(mat[i][j+1], mat[i+1][j], mat[i][j]))

    #print("before: )
    #print(DataFrame(mat))
    print("Tafavot goftar shoma ba adad {0}: {1}".
        format(template_name, 
        mat[len(mfccTempl[template_name][0])][len(mfccTest[0])]))

    return mat[len(mfccTempl[template_name][0])][len(mfccTest[0])]

# main:
mfccTempl = dict()

for i in range(0, 10):
    y, sr = librosa.load("./wavSignals/" + str(i) + ".wav")
    mfccTempl[str(i)] = librosa.feature.mfcc(y, sr)

mfccTest = khandanAudioTest()

candid_val = sys.maxsize
candid_name = ""
for template_name, _ in mfccTempl.items():
    dtw_ret_val = dtw(template_name, mfccTest)
    if dtw_ret_val < candid_val:
        candid_name = template_name
        candid_val = dtw_ret_val

print("Be nazar mirese ke shoma kalameye moghabel ro gofte bashid: {0}"
    .format(candid_name))

