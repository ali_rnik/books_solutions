## About Project
 This is an not accurate implementation of "Closed-loop halting control" which 
it's problem explained in article.pdf. In a nutshell, there is a robot walks 
through a line (or maybe rolling) and it have to wait on a certain 
point (which is zero), so the left part of point number zero is the negative 
points and the right part is positive points, Now the problem is with given 
speed and velocity at every points (well not every points) of this control 
line, how much traction force is needed to use to cause robot stops at point 
zero.

## Getting it to wok
 First of all you need python3 PL interpreter, and then you have to install 
sci-kit fuzzy, matplotlib, numpy and jupyter-notebook. In Archlinux sci-kit 
fuzzy is available in Arch User Repositories and other packages could be 
installed within the official packages repositories:
```
pacman -S python-matplotlib python-numpy jupyter
```

Now you can run jupyter-notebook  and there is a binary file with this name and jupyter-notebook web user interface comes up, Now try to open article_implementation.ipynb and after you run the example all diagrams would drawn.


## Project tree

<li><dl class="first docutils">
<dt>Antecednets (Inputs)</dt>
<dd><ul class="first last">
<li><dl class="first docutils">
<dt><cite>position</cite></dt>
<dd><ul class="first last simple">
<li>Universe (ie, crisp value range): How far position error is from zero 
point? it would be between -4 and +4. </li>
<li>PNL(negative large), PNS(nega-tive small), PZ(zero), PPS(positive small), 
and PPL(positive large)</li>
</ul>
</dd>
</dl>
</li>
<li><dl class="first docutils">
<dt><cite>velocity error</cite></dt>
<dd><ul class="first last simple">
<li>Universe: How much velocity error is at current positin? it would be 
between -4 and +4 </li>
<li>Fuzzy set (ie, fuzzy value range): VNL(negative large), 
VNS(nega-tive small), VZ(zero), VPS(positive small), and VPL(positive large)</li>
</ul>
</dd>
</dl>
</li>
</ul>
</dd>
</dl>
</li>
<li><dl class="first docutils">
<dt>Consequents (Outputs)</dt>
<dd><ul class="first last">
<li><dl class="first docutils">
<dt><cite>traction force</cite></dt>
<dd><ul class="first last simple">
<li>Universe: How much traction force must we use to control the robot to wait
on point zero? it is between -500N to +500N</li>
<li>Fuzzy set: UNX(negative extralarge), UNL(negative large), 
UNS(negative small), UPS(positive small), UPL(positive large), and 
UPX(positive extra large)</li>
</ul>
</dd>
</dl>
</li>
</ul>
</dd>
</dl>
</li>
<li><dl class="first docutils">
<dt>Rules</dt>
<dd><ul class="first last simple">
<img src=rules.png> </img>
</ul>
</dd>
</dl>
</li>
<li><dl class="first docutils">
<dt>Usage</dt>
<dd><ul class="first last">
<li><dl class="first docutils">
<dt>Consider these are my inputs:</dt>
<dd><ul class="first last simple">
<li>the position error is 0, and</li>
<li>the velocity error is 0,</li>
</ul>
</dd>
</dl>
</li>
<li><dl class="first docutils">
<dt>The fuzzy control system will recommend me:</dt>
<dd><ul class="first last simple">
<li>to use 0 traction force to stop the robot in zero position point.</li>
</ul>
</dd>
</dl>
</li>
</ul>
</dd>
</dl>
</li>