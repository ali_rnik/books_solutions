import numpy as np
import collections
import matplotlib.pyplot as plt
import pandas
from graphviz import Digraph
from sklearn.neural_network import MLPClassifier

col_names = ['isHappy', 'infoAvail', 'houseCost', 'schoolQuality', 'policeTrust', 'cityMaintain', 'eventAvail']
table = pandas.read_csv("someville_happiness.csv", header=None, names=col_names)

input_col = col_names[1:]
answer_col = col_names[0:1]
inputs = table[input_col].to_numpy()
answer = table[answer_col].to_numpy().flatten()

table.head()

def make_mlp(activation, layers, backpro_alg):

    g = Digraph(comment=("diagram with " + activation + " AF and " + 
                         backpro_alg + "BP algorithm."))
    g.graph_attr['labeldistance'] = '0'
    g.graph_attr['nodesep'] = '0'
    g.graph_attr['ranksep'] = '4'    

    mlp = MLPClassifier(activation=activation, 
                        solver=backpro_alg, 
                        hidden_layer_sizes=layers, 
                        random_state=1,
                        max_iter=6000)

    mlp.fit(inputs, answer)
    
    for s in input_col:
        g.node(s)
    
    # weights of layers
    for i in range(len(mlp.coefs_)):
        for j in range(len(mlp.coefs_[i])):
            for k in range(len(mlp.coefs_[i][j])):
                if i == 0:
                    g.node(str(i+1)+"-"+str(k+1))
                    g.edge(input_col[j], str(i+1)+"-"+str(k+1), str(float("{:.2f}".format(mlp.coefs_[i][j][k]))))
                else:
                    g.node(str(i+1)+"-"+str(k+1))
                    g.edge(str(i)+"-"+str(j+1), str(i+1)+"-"+str(k+1), str(float("{:.2f}".format(mlp.coefs_[i][j][k]))))                    
    
    guessed = mlp.predict(inputs)
    
    print("answers:", answer)
    print("guesses:", guessed)
    
    dif = 0
    for i in range(len(answer)):
        if answer[i] != guessed[i]:
            dif += 1

    err = dif/len(answer) * 100
    print("Error:", err)
    g.node(str(len(mlp.coefs_))+"-"+str(1), "Error %: " + str(err))

    return g


mfa = ["identity", "logistic", "tanh", "relu"]
bpa = ["lbfgs", "sgd", "adam"]

for mf in mfa:
    for bp in bpa:
        g = make_mlp(mf, len(inputs), bp)
        g.render(str(mf)+"_"+str(bp), view=True) 
