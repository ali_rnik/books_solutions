# Multi Layer Preceptron for Somerville data analysis

This is a program for anaylising Somerville dataset with various MLP methods. 

The Somerville dataset consist of 7 attributes with the following description:


  * D = decision attribute (D) with values 0 (unhappy) and 1 (happy)

  * X1 = the availability of information about the city services

  * X2 = the cost of housing

  * X3 = the overall quality of public schools

  * X4 = your trust in the local police

  * X5 = the maintenance of streets and sidewalks

  * X6 = the availability of social community events

  * Attributes X1 to X6 have values 1 to 5.


### Methods which we can use for activation function are:
  * ‘identity’, no-op activation, useful to implement linear bottleneck, returns f(x) = x
  * ‘logistic’, the logistic sigmoid function, returns f(x) = 1 / (1 + exp(-x)).
  * ‘tanh’, the hyperbolic tan function, returns f(x) = tanh(x).
  * ‘relu’, the rectified linear unit function, returns f(x) = max(0, x)


### Methods which we can use for computing backpropagation:
  * ‘lbfgs’ is an optimizer in the family of quasi-Newton methods.
  * ‘sgd’ refers to stochastic gradient descent.
  * ‘adam’ refers to a stochastic gradient-based optimizer proposed by Kingma, Diederik, and Jimmy Ba


#### References:
[https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html#sklearn.neural_network.MLPClassifier.predict_proba
](https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html#sklearn.neural_network.MLPClassifier.predict_proba)

[https://archive.ics.uci.edu/ml/datasets/Somerville+Happiness+Survey#](https://archive.ics.uci.edu/ml/datasets/Somerville+Happiness+Survey#)