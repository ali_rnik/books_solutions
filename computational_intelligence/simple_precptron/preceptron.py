import random

class Preceptron:
    weights = []
    learn_const = 0.02
    
    def __init__(self, n):
        for i in range(n):
            self.weights.append(random.uniform(-1, 1))
        self.weights.append(1) # bias

    def activate(self, sum):
        if sum >= 0:
            return 1
        else:
            return -1
        
    def feedforward(self, inputarr):
        sum = 0
        for i in range(len(self.weights)):
            sum += (inputarr[i] * self.weights[i])
        return self.activate(sum)
        
    def train(self, inputarr, ans):
        guess_ans = self.feedforward(inputarr)
        error = ans - guess_ans
        for i in range(len(self.weights)):
            self.weights[i] += (error * inputarr[i] * self.learn_const)

    def guess(self, inputarr):
        guess_ans = self.feedforward(inputarr)
        if guess_ans == 1:
            print("The sum of", inputarr, "is a Positive number!")
        else:
            print("The sum of", inputarr, "is a Negative number!")
    
    def clr_weights(self):
        for i in range(len(self.weights)):
            self.weights[i] = random.choice([-1, 1])

# Namayesh weights ghabl az Amoozesh:
print("weights ghabl az Amoozesh:", precept.weights)


# Hads ghabl az Amoozesh:
print("Hads 1 ghabl az Amoozesh:")
precept.guess([-3, -2, 1])
precept.clr_weights()

print("Hads 2 ghabl az Amoozesh:")
precept.guess([4, -2, 1])
precept.clr_weights()

print("Hads 3 ghabl az Amoozesh:")
precept.guess([1, 6, 1])
precept.clr_weights()

print("Hads 4 ghabl az Amoozesh:")
precept.guess([1, 6, 1])
precept.clr_weights()

print("Hads 4 ghabl az Amoozesh:")
precept.guess([1, 6, 1])
precept.clr_weights()

# Amoozesh
train_testcases = 1000
for i in range(train_testcases):
    x = random.randrange(-10000, 10000)
    y = random.randrange(-10000, 10000)
    
    if x + y >= 0:
        ans = 1
    else:
        ans = -1
    
    print(x, y)
    precept.train([x, y, 1], ans)   

# Namayesh weights bad az Amoozesh:
print("weights bad az Amoozesh:", precept.weights)

#Hads bad az Amoozesh:
print("Hads 1 bad az Amoozesh:")
precept.guess([-3, -2, 1])

print("Hads 2 bad az Amoozesh:")
precept.guess([4, -2, 1])

print("Hads 3 bad az Amoozesh:")
precept.guess([-1, -1, 1])

print("Hads 4 bad az Amoozesh:")
precept.guess([1, 6, 1])

print("Hads 4 bad az Amoozesh:")
precept.guess([1, -1, 1])

print("Hads 5 bad az Amoozesh:")
precept.guess([0, 0, 1])
