## Single Layer Preceptron

### About this example
  
This example is about to say whether the sum of some integers are positive or

negative, but without the direct power of conditional unit because its possible

to check whether sum is positive or negative with the following code snippet:
```python
if sum >= 0:
    print("positive")
else:
    print("negative")
```
Now instead of using the power of conditional unit directly, we want to build a

system which we could teach it, I mean to give it some input and the correct

output and expect it to get us an appropriate answer next time we give it arbitrary

input.


### Reference
https://natureofcode.com/book/chapter-10-neural-networks/