#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Author: Ali R. Nik

#  Ebteda be khatere finglish neveshtan ozr khahi mikonam 
#  ( be dalil support nakardan rast chin va chapchin)
#Tafavot rang dar 3 tarkib motafavet rgb, bgr va gbr in 
# ast ke meghdar adadi har rang, ghodrat tabesh aan
# rang ra moshakhas mikonad va vaghti ghodrat haye rang
# tabideh shode raa jabeja konim, entezar
# darim ke rang haye tasvir niz taghir konad.


# In[2]:


import matplotlib
from matplotlib.pyplot import figure, show
import matplotlib.image as mpimg

rgb = mpimg.imread("image.png")

fg = figure(figsize=(15, 4))
ax = fg.subplots(1, 3, sharey=True)

ax[0].imshow(rgb, origin="upper")
ax[0].set_title("RGB")

bgr = rgb[..., ::-1]
ax[1].imshow(bgr, origin="upper")
ax[1].set_title("BGR")

gbr = rgb[..., [2, 0, 1]]
ax[2].imshow(gbr, origin="upper")
ax[2].set_title("GBR")

fg.suptitle("")
fg.tight_layout()
show()

