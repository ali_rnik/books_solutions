#!/usr/bin/env python
# coding: utf-8

# In[1]:


from matplotlib.pyplot import imread
import matplotlib.pyplot as plt
from scipy.ndimage.filters import convolve
import numpy as np


# In[2]:


laplacian = np.array([ [0, -1, 0], [-1, 5, -1], [0, -1, 0] ])
img = imread("my_image.jpg")
img = img.astype(np.int16)

plt.imshow(img)
plt.show()


# In[3]:


channels = []
for channel in range(3):
    res = convolve(img[:,:,channel], laplacian)
    channels.append(res)

img = np.dstack((channels[0], channels[1], channels[2]))
plt.imshow(img)

plt.show()

