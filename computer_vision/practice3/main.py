#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Author: Ali R. Nik

# Ebteda ozr khahi mikonam baraye neveshtar be soorat finglish
# (Be dalil adam support in Document as Farsi)

#  function f(x) = 255 - x , baraye makoos kardan ghodrat tabesh 
# dar har noghte ast. baraye nemone agar noghteyi ghodrat tabesh 
# an 200 bashad, bad az emal f(x) ghodrat tabeshe an be 55 taghir 
# mikonad.


# In[2]:


import matplotlib
from matplotlib.pyplot import figure, show
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


# In[3]:


my_image = mpimg.imread("my_image.png")

fg = figure(figsize=(15, 4))
ax = fg.subplots(1, 3, sharey=True)

ax[0].imshow(my_image, origin="upper")
ax[0].set_title("Tasvir Avalieh")

gray = Image.open('my_image.png').convert("1")
ax[1].imshow(gray, origin="upper")
ax[1].set_title("Tasvir Grayscale")

fx_applied = gray.point(lambda i: 255-i)
ax[2].imshow(fx_applied, origin="upper")
ax[2].set_title("fx_applied")

fg.suptitle("")
fg.tight_layout()
show()

