#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


mat =  np.random.randint(0, 1000, size=(5, 5))
print(mat);


# In[3]:


Min = mat.min();
Max = mat.max();

newMin = 0;
newMax = 255;


# In[4]:


for i in range (0, 5):
    for j in range(0, 5):
        mat[i][j] = ((mat[i][j] - Min) * ((newMax-newMin)/(Max-Min))) + newMin;

print(mat)

