#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Author: Ali Ramezanian Nik
from scipy.ndimage import gaussian_filter
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
import matplotlib


# In[ ]:


fig = plt.figure(figsize=(10,10))
place1 = fig.add_subplot(551)
place2 = fig.add_subplot(552)
place3 = fig.add_subplot(553)
place4 = fig.add_subplot(554)
place5 = fig.add_subplot(555)

equal = matplotlib.image.imread("./equal.png")
subtract = matplotlib.image.imread("./subtract.jpg")
mypic = matplotlib.image.imread("./image.jpg")

result1 = gaussian_filter(mypic, sigma=1)
subtracted = mypic - result1

place1.imshow(mypic)
place2.imshow(subtract)
place3.imshow(result1)
place4.imshow(equal)
place5.imshow(subtracted)

plt.show()

