#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Author: Ali R. Nik
# 
#  bad az normalize kardan tasvir, maghadir pixel ha
# beyn 0 ta 1 mishavand, midanim ke har che be 1 nazdik
# tar shavim logarithm meghdar kamtari khahad dasht. 
# pas ghesmat haye por rang tasvir kamrang tar khahand shod.


# In[2]:


import matplotlib
import cv2
from matplotlib.pyplot import figure, show
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np


# In[3]:


pout = cv2.imread("pout.tif")

fg = figure(figsize=(15, 4))
ax = fg.subplots(1, 2, sharey=True)

ax[0].imshow(pout, origin="upper")
ax[0].set_title("pout")

np.seterr(divide = 'ignore') 
c = 255 / np.log(1 + np.max(pout)) 
log_image = c * (np.log(pout + 1)) 
log_image = np.array(log_image, dtype = np.uint8) 

ax[1].imshow(log_image, origin="upper")
ax[1].set_title("logarithm of pout")

fg.suptitle("")
fg.tight_layout()
show()

