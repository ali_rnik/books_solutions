#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
import os
import copy


# In[2]:


def image_detect_and_compute(detector, img_name):
    img = cv2.imread(img_name)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des
    

def draw_image(image):

    plt.figure(figsize=(16, 16))
    plt.imshow(image); 
    plt.show();

def calculate_distance(detector, img1_name, img2_name, nmatches=10):
    img1, kp1, des1 = image_detect_and_compute(detector, './train/'+img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, './test/'+img2_name)
    
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    distance = 0
    matches = sorted(matches, key = lambda x: x.distance) #Best come first.
    for i in range(0, nmatches):
        distance += matches[i].distance;
    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches], img2, flags=2)
    return distance, img_matches;


# In[ ]:


for ipath in os.listdir('./train'):
    print()
    print("##########################################################")
    print()
    
    min_dist = 1000000;
    min_img = 0.0;
    for jpath in os.listdir('./test'):

        print("Moghayese 2 Aks " + ipath + " va " + jpath)        
        orb = cv2.ORB_create()
        distance, image = calculate_distance(orb, ipath, jpath)
        print("Meghdar tafavot adadi:(Kamtar behtar ast): ", distance);
        print()

        if distance < min_dist:
            min_dist = distance;
            min_img = copy.deepcopy(image);
    print("Rasm nemoodar graphici moshabeh tarin aks, va moghayese an ba aks marja.")
    print("Tafavot ba aks marja: ", min_dist);

    draw_image(min_img);
        

