import numpy as np
import pandas
import statsmodels.api as smodel

col_names = ['num_preg', 'glucose_conc', 'diastolic_bp', 'skin_thickness', 'insulin', 'bmi', 'diab_pred', 'age', 'skin', 'diabetes']
main_table = pandas.read_csv("diabetes_diagnosis.csv", header=None, names=col_names)

gheyr_vabaste_col = col_names[0:len(col_names)-1]
gheyr_vabaste = main_table[gheyr_vabaste_col]

vabaste_col = col_names[len(col_names)-1:len(col_names)]
vabaste = main_table[vabaste_col]

gheyr_vabaste.head()

vabaste.head()

var_gheyr_vabaste = gheyr_vabaste[["glucose_conc", "insulin", "bmi", "age"]]
var_vabaste = vabaste["diabetes"]

model = smodel.OLS(var_vabaste, var_gheyr_vabaste).fit() # min(fasele har noghte az khat regression)^2
pishbini = model.predict(var_gheyr_vabaste)

model.summary()
