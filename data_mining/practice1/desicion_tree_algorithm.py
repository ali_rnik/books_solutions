import pandas
import matplotlib.pyplot as plt
from graphviz import Digraph

LEFT = 0
RIGHT = 1

""" 
  function that calculate minimum weighted average gini impurity on 
 numeric column of data and divide it into two part which the average
 which it has divided is returned.  
"""
def calc_min_gini(table, name):
    table_sz = table[name].size
    gini = 1
    selected_avg = 0
    
    for i in range(table_sz-1):
        y_before = table[0:i+1].loc[table["diabetes"] == True, "diabetes"].size
        n_before = table[0:i+1].loc[table["diabetes"] == False, "diabetes"].size
        y_after = table[i+1:].loc[table["diabetes"] == True, "diabetes"].size
        n_after = table[i+1:].loc[table["diabetes"] == False, "diabetes"].size
        
        gini_before = (1 
                       - (y_before/(y_before+n_before))**2 
                       - (n_before/(y_before+n_before))**2)
        gini_after =  (1
                       - (y_after/(y_after+n_after))**2
                       - (n_after/(y_after+n_after))**2)
        
        a = ((i+1)/table_sz)*gini_before
        b = ((table_sz - (i+1))/table_sz)*gini_after
        weighted_gini_avg = a + b
        
        avg = (table.iat[i, 0] + table.iat[i+1, 0])/2

        if weighted_gini_avg <= gini:
            gini = weighted_gini_avg
            selected_avg = avg
            
        #print("weighted_gini_avg: " , weighted_gini_avg)
        
    return gini, selected_avg, name

"""
 sort columns and calculate gini for each columns
and return the minimum gini which divide table.
"""
def find_divider(selected_cols, table):
    sorted_table = dict()
    gini_output = dict()
    minimum_gini = tuple((1, 1, ""))

    for s in selected_cols:
        if s == "diabetes":
            continue
        sorted_table[s] = table[[s, "diabetes"]].sort_values(s)
        gini_output[s] = calc_min_gini(sorted_table[s], s)
        if gini_output[s][0] <= minimum_gini[0]:
            minimum_gini = gini_output[s]

    return minimum_gini

"""
  divide the data_table into two table
 which separated with best impurity divider.
"""
def divide_by(gini_data, table):
    new_table = table.sort_values(by=[gini_data[2]])
    low = new_table[new_table[gini_data[2]] <= gini_data[1]]
    high = new_table[new_table[gini_data[2]] > gini_data[1]]
    
    return low, high


def node_info(node, divider, tree):
    print("this node name:", tree["name"])
    print(divider)
    print()
    wholel = node[LEFT]["diabetes"].size
    truel = node[LEFT].loc[node[LEFT]["diabetes"] == True, "diabetes"].size
    if wholel == 0:
        dial = 0
    else:
        dial = truel/wholel
    print(divider[2]," <= ", divider[1], " ? YES :")
    print("Total people: ", wholel)
    print("With diabetes: ", truel)
    print("Without diabetes: ", wholel-truel)
    print("Percentage of having diabetes: ", dial * 100)
    print()
    wholer = node[RIGHT]["diabetes"].size
    truer = node[RIGHT].loc[node[RIGHT]["diabetes"] == True, "diabetes"].size
    if wholer == 0:
        diar = 0
    else:
        diar = truer/wholer
    print(divider[2]," <= ", divider[1], " ? NO :")
    print("Total people: ", wholer)
    print("With diabetes: ", truer)
    print("Without diabetes: ", wholer-truer)
    print("Percentage of having diabetes: ", diar * 100)    
    print()
    print()
    
    label = (tree["name"] + "\n" 
             + "gini:" + str(float("{:.3f}".format(divider[0]))) + "\n" 
             + str(divider[2]) + " <= " + str(divider[1]))
    
    g.node(tree["name"], label)
    g.node(tree["child"][LEFT])
    g.node(tree["child"][RIGHT])
    
    label = ("YES \ntotal:" + str(wholel) + "\n" 
        + str(truel) + "/" + str(wholel-truel) + "\n" 
        + str(float("{:.2f}".format(dial*100))) + "%")
    g.edge(tree["name"], tree["child"][LEFT], label)
    
    label = ("NO \ntotal:" + str(wholer) + "\n" 
        + str(truer) + "/" + str(wholer-truer) + "\n" 
        + str(float("{:.2f}".format(diar*100))) + "%")
    g.edge(tree["name"], tree["child"][RIGHT], label)
    

def tree_maker(table, columns, name, depth, tree, parent):
    #if depth > 3:
    #   return
    
    divider = find_divider(columns, table)
    new_table = divide_by(divider, table)
    child = [name + "_L", name + "_R"]
    
    
    tree[name] = { "name" : name,
                 "table": new_table, 
                 "divider": divider,
                 "parent": parent,
                 "columns": columns.copy(),
                 "child": child}
    
    node_info(new_table, divider, tree[name])
    
    columns.remove(divider[2])
    
    if divider[0] < 0.1:
        return
    
    if len(columns) <= 2:
        return
    
    tree_maker(new_table[LEFT], columns.copy(), child[LEFT], depth+1, tree, name)
    tree_maker(new_table[RIGHT], columns.copy(), child[RIGHT], depth+1, tree, name)

# naming columns
col_names = ['num_preg', 'glucose_conc', 'diastolic_bp', 'skin_thickness', 'insulin', 'bmi', 'diab_pred', 'age', 'skin', 'diabetes']
main_table = pandas.read_csv("diabetes_diagnosis.csv", header=None, names=col_names)

main_table.head()

main_table.describe()

# fill missing datas with median
missing_colnames = col_names[1:len(col_names)-1].copy()
for s in missing_colnames:
    main_table.loc[main_table[s] == 0, s] = main_table[s].median()

main_table.head()

# making the decision tree and visualize with graphviz

g = Digraph(comment='Diabetes decision tree.')
tree = dict()

tree_maker(main_table, col_names.copy(), "root", 0, tree, "NULL")

g.render('decision_tree', view=True) 
