import pandas
import matplotlib.pyplot as plt

# naming columns
col_names = ['num_preg', 'glucose_conc', 'diastolic_bp', 'skin_thickness', 'insulin', 'bmi', 'diab_pred', 'age', 'skin', 'diabetes']
main_table = pandas.read_csv("diabetes_diagnosis.csv", header=None, names=col_names)

def discrete(labels, q, column):
    main_table[column + "_ds"] = pandas.qcut(main_table[column], 
                                                   q=q, 
                                                   labels=labels,
                                                   duplicates='drop')

labels = ['veryLow', 'low', 'medium', 'high', 'veryHigh']
q = [0, 0.2, 0.4, 0.6, 0.8, 1]
discrete(labels, q, col_names[0])
discrete(labels, q, col_names[1])
discrete(labels, q, col_names[2])

labels = ['veryLow', 'low', 'medium', 'high']
q = [0, 0.2, 0.4, 0.6, 0.8, 1]
discrete(labels, q, col_names[3])

labels = ['veryLow', 'low', 'medium']
q = [0, 0.2, 0.4, 0.6, 0.8, 1]
discrete(labels, q, col_names[4])

labels = ['veryLow', 'low', 'medium', 'high', 'veryHigh']
q = [0, 0.2, 0.4, 0.6, 0.8, 1]
discrete(labels, q, col_names[5])
discrete(labels, q, col_names[6])
discrete(labels, q, col_names[7])

labels = ['veryLow', 'low', 'medium', 'high']
q = [0, 0.2, 0.4, 0.6, 0.8, 1]
discrete(labels, q, col_names[8])

main_table.to_csv("discrete.csv")
main_table.head(50)


