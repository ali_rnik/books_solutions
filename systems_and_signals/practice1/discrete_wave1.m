x = -7:0.2:7
y = stem(tan(x));

title("discreteWave1");
xlabel("x");
ylabel("y[x] = tan(x)");
print -djpg discrete_wave1.png