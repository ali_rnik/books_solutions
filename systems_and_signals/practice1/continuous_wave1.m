x = -7:7;
y = x.^3;
plot(y);

title ("continuousWave1");
xlabel ("x");
ylabel ("y(x) == x^3");
print -djpg continuous_wave1.png