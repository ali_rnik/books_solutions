x = -7:0.3:7;
y = stem(x.^2);

title("discreteWave2");
xlabel("x");
ylabel("y[x] == x^2");
print -djpg discrete_wave2.png