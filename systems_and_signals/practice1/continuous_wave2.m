x = -7:7;
y = sin(cos(x));
plot(y);

title ("continuousWave2");
xlabel ("x");
ylabel ("y(x) == sin(cos(x))");
print -djpg continuous_wave2.png